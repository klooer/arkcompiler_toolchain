# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//arkcompiler/toolchain/toolchain.gni")

config("hiviewdfx_config") {
  defines = []
  if (enable_dump_in_faultlog) {
    defines += [ "ENABLE_DUMP_IN_FAULTLOG" ]
  }
  if (enable_bytrace) {
    defines += [ "ENABLE_BYTRACE" ]
    cflags_cc = [ "-Wno-gnu-zero-variadic-macro-arguments" ]
  }
  if (enable_hitrace) {
    defines += [ "ENABLE_HITRACE" ]
  }
  if (enable_hilog) {
    defines += [ "ENABLE_HILOG" ]
  }

  include_dirs = [ "$hilog_root/include" ]
}

config("ark_platform_config") {
  configs = [ ":hiviewdfx_config" ]
  defines = []
  if (is_ohos) {
    defines += [
      "OHOS_PLATFORM",
      "UNIX_PLATFORM",
    ]
  } else if (is_mingw) {
    defines += [ "WINDOWS_PLATFORM" ]
  } else if (is_mac) {
    defines += [
      "MAC_PLATFORM",
      "UNIX_PLATFORM",
    ]
  } else if (target_os == "android") {
    defines += [
      "ANDROID_PLATFORM",
      "UNIX_PLATFORM",
    ]
  } else if (target_os == "ios") {
    defines += [
      "UNIX_PLATFORM",
      "IOS_PLATFORM",
    ]
  } else {
    defines += [ "UNIX_PLATFORM" ]
  }
}

ohos_source_set("ark_debugger_static") {
  deps = [ sdk_libc_secshared_dep ]
  configs = [ sdk_libc_secshared_config ]
  defines = []

  if (is_mingw || is_mac) {
    cflags = [ "-std=c++17" ]
  }

  include_dirs = [
    "$toolchain_root",
    "$toolchain_root/websocket",
  ]

  if (target_os == "android") {
    aosp_deps = [ "shared_library:liblog" ]
  }

  deps += [ "$toolchain_root/websocket:websocket" ]
  sources = [
    "inspector.cpp",
    "library_loader.cpp",
    "log_wrapper.cpp",
    "ws_server.cpp",
  ]

  # hiviewdfx libraries
  external_deps = hiviewdfx_ext_deps
  deps += hiviewdfx_deps

  configs += [ ":ark_platform_config" ]
}

ohos_shared_library("ark_debugger") {
  deps = [ ":ark_debugger_static" ]

  if (is_mingw) {
    ldflags = [
      "-lwsock32",
      "-lws2_32",
    ]
  }

  part_name = "toolchain"
  subsystem_name = "arkcompiler"
}

ohos_shared_library("connectserver_debugger") {
  deps = [ sdk_libc_secshared_dep ]
  configs = [ sdk_libc_secshared_config ]
  defines = []

  if (is_mingw || is_mac) {
    cflags = [ "-std=c++17" ]
  }

  configs += [ ":ark_platform_config" ]

  # hiviewdfx libraries
  external_deps = hiviewdfx_ext_deps
  deps += hiviewdfx_deps

  deps += [ "$toolchain_root/websocket" ]

  include_dirs = [
    "$toolchain_root/websocket",
    "$toolchain_root/inspector",
  ]

  sources = [
    "connect_inspector.cpp",
    "connect_server.cpp",
    "log_wrapper.cpp",
  ]

  subsystem_name = "arkcompiler"
  part_name = "toolchain"
}
